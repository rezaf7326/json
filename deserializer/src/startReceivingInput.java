import java.util.Scanner;

public class startReceivingInput extends JSONItems{
    public static void start () {
        Scanner input = new Scanner(System.in);
        String JSON = "";
        String newLine = "";
        System.out.println("Enter a json string ending with a line only containing a zero --> \"0\".");
        while (!newLine.equals("0")) {
            JSON = JSON + newLine;
            newLine = input.nextLine();
        }
        //JSON = JSON.replaceAll("\\s+", "");
        JSONItems.JSON = JSON.replaceAll("\n+", "");
        Deserializer.deserializer();
        for (int index = 0; index < firstPartList.size(); index++) {
            System.out.print(firstPartList.get(index) + "  ");
            System.out.println(secondPartList.get(index));
        }
    }
}
