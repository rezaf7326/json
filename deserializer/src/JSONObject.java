import java.util.ArrayList;

public class JSONObject extends JSONItems{
    public static void makeObject (int initialIndex, int finalIndex, boolean number, boolean bool) {
        if (bool) {
            objectArrayList.add(Boolean.parseBoolean(JSON.substring(initialIndex, finalIndex).trim()));
            return;
        }
        if (number) {
            if (Deserializer.isItDouble(initialIndex, finalIndex)) {
                objectArrayList.add(Double.parseDouble(JSON.substring(initialIndex, finalIndex).trim()));
            }else {
                objectArrayList.add(Integer.parseInt(JSON.substring(initialIndex, finalIndex).trim()));
            }
        }else {
            objectArrayList.add(JSON.substring(initialIndex, finalIndex));
        }
    }

    private static int fromIndex = 0;
    public static void makeSecondPart () {
        ArrayList <Object> temp = new ArrayList<Object>(objectArrayList.subList(fromIndex, objectArrayList.size()));
        secondPartList.add(temp);
        fromIndex = objectArrayList.size();
    }

    public static void makeSecondPartCurlyAtPosition (int leftListIndexPosition) {
        ArrayList <Object> temp = new ArrayList<Object>(objectArrayList.subList(fromIndex, objectArrayList.size()));
        secondPartList.add(leftListIndexPosition ,temp);
        fromIndex = objectArrayList.size();
    }
}
