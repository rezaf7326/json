public class charReader extends JSONItems{

    private static Integer indexCounter = 0;
    private static Integer quoteKeeper = 0;
    private static boolean inQuote = false;
    private static boolean insideQuote () {
        for (int index = quoteKeeper; index < indexCounter; index++) {
            if (JSON.charAt(index) == '\"') {
                inQuote = !inQuote;
                if (inQuote) {
                    quoteKeeper = indexCounter-1;
                }
            }
        }
        return inQuote;
    }

    public static char isNumber () {
        String numSample = "0987654321";
        if (numSample.contains(JSON.charAt(indexCounter) + ""))
            return 'n';
        return '\0';
    }

    public static boolean inQuote () {
        return inQuote;
    }

    public static int quoteBeginning (){
        return quoteKeeper;
    }

    public static void updateQuoteKeeper () {
        quoteKeeper = indexCounter+1;
        inQuote = false;
    }

    private static void skipWhiteSpace () {
        if (!insideQuote()) {
            //do {
                indexCounter++;
           // }while (JSON.charAt(indexCounter) ==' ');
        }else if (indexCounter < JSON.length()){
            if (JSON.indexOf('\"', indexCounter) == -1) {throwException();}
            indexCounter = JSON.indexOf('\"',
                    indexCounter);
        }
    }

    public static String theCharacter () {
        skipWhiteSpace();
        char ch = JSON.charAt(indexCounter);
        if (isNumber() == 'n')
            ch = 'N';
        switch (ch) {
            case '\"':
                while (!inQuote) {
                    skipWhiteSpace();
                }
                inQuote = false;
                return "quotation";
            case '{':
                inQuote = false;
                return "curlyOpen";
            case '[':
                inQuote = false;
                return "bracketOpen";
            case ':':
                inQuote = false;
                return "colon";
            case ',':
                inQuote = false;
                return "comma";
            case 'f':
            case 't':
                inQuote = false;
                return "boolean";
            case 'N':
                inQuote = false;
                return "number";
            default:
                return "\0";
        }
    }

    public static int index () {
        return indexCounter;
    }

    public static void updateCounter (int newCounter) {
        indexCounter = newCounter;
    }

    public static void nextCharIndex () {
        skipWhiteSpace();
    }

    public static boolean nextExists () {
        return indexCounter < JSON.length()-1;
    }

    public static void throwException() {
        throw new RuntimeException("Input String is not a valid JSON, error index: " + indexCounter);
    }
}
