public class Deserializer extends JSONItems{

    private static boolean leftHandSide = true;

    private static void quote (boolean leftSideQuote) {
      if (leftSideQuote) {
          firstPartList.add(JSON.substring(charReader.quoteBeginning(), charReader.index()+1));
      }else {
          JSONObject.makeObject(charReader.quoteBeginning(), charReader.index()+1, false, false);
          JSONObject.makeSecondPart();
      }
    }

    public static boolean isItDouble (int first, int last) {
        return JSON.substring(first, last).contains(".");
    }

    private static boolean itIsNumber (int first, int last) {
        String doubleSample = "0123456789.";
        for (int index = first; index < last; index++) {
            if (!(doubleSample.contains(JSON.charAt(index) + ""))) {
                return false;
            }
        }
        return true;
    }

    private static void curly () {
        boolean leftQuote = true;
        while (true) {
            charReader.nextCharIndex();
            char ch = JSON.charAt(charReader.index());
            if (charReader.isNumber() == 'n' && !charReader.inQuote())
                ch = 'N';
            switch (ch) {
                case '\"':
                    while (!charReader.inQuote()) {
                        charReader.nextCharIndex();
                    }
                    quote(leftQuote);
                    charReader.updateQuoteKeeper();
                    break;
                case '{':
                    int curlyBegins = charReader.index();
                    int leftSideListIndex = firstPartList.size()-1;
                    leftQuote = true;
                    curly();
                    addAtPositionAndClose(curlyBegins, leftSideListIndex);
                    break;
                case '}':
                    leftQuote = true;
                    return;
                case '[':
                    leftQuote = true;
                    bracket();
                    break;
                case ':':
                    leftQuote = false;
                    break;
                case ',':
                    leftQuote = true;
                    break;
                case 't':
                case 'f':
                    booleanCase();
                    JSONObject.makeSecondPart();
                    break;
                case 'N':
                    numberCase();
                    JSONObject.makeSecondPart();
                default:
                    break;
            }
        }
    }

    private static void addAtPositionAndClose (int curlyStart, int leftListPosition) {
      JSONObject.makeObject(curlyStart, charReader.index()+1, false, false);
      JSONObject.makeSecondPartCurlyAtPosition(leftListPosition);
  }

    private static void bracket () {
        int indexCounter = charReader.index();
        int bracketClose = JSON.indexOf(']', charReader.index());
        if (bracketClose == -1) {charReader.throwException();}
        for (int nextIndex = charReader.index(); nextIndex <= bracketClose; nextIndex++) {
            if (JSON.charAt(nextIndex) == '{') {
                indexCounter = nextIndex;
                if (JSONItems.JSON.indexOf('}', indexCounter) == -1) {charReader.throwException();}
                nextIndex = JSONItems.JSON.indexOf('}', indexCounter)+1;
                JSONObject.makeObject(indexCounter, nextIndex, false, false);
                indexCounter = nextIndex+1;
            }else if (JSON.charAt(nextIndex) == ',' || JSON.charAt(nextIndex) == ']') {
                if (!itIsNumber(indexCounter +1, nextIndex)) {
                    JSONObject.makeObject(indexCounter + 1, nextIndex, false, false);
                }else {
                    JSONObject.makeObject(indexCounter + 1, nextIndex, true, false);
                }
                indexCounter = nextIndex;
            }
        }
        JSONObject.makeSecondPart();
        charReader.updateCounter(bracketClose);
    }

    private static void numberCase () {
        int numStart = charReader.index();
        if (JSON.indexOf(',', numStart) != -1 && JSON.indexOf('}', numStart) != -1) {
            int minCommaOrCurly = Math.min(JSON.indexOf(',', numStart), JSON.indexOf('}', numStart));
            JSONObject.makeObject(numStart, minCommaOrCurly, true, false);
            charReader.updateCounter(minCommaOrCurly-1);
        }else if (JSON.indexOf(',', numStart) != -1) {
            int comma = JSON.indexOf(',', numStart);
            JSONObject.makeObject(numStart, comma, true, false);
            charReader.updateCounter(comma-1);
        }else if (JSON.indexOf('}', numStart) != -1) {
            int curlyClose = JSON.indexOf('}', numStart);
            JSONObject.makeObject(numStart, curlyClose, true, false);
            charReader.updateCounter(curlyClose-1);
        }else {
            charReader.throwException();
        }
    }

    private static void booleanCase () {
        int boolStart = charReader.index();
        if (JSON.indexOf(',', boolStart) != -1 && JSON.indexOf('}', boolStart) != -1) {
            int minCommaOrCurly = Math.min(JSON.indexOf(',', boolStart), JSON.indexOf('}', boolStart));
            JSONObject.makeObject(boolStart, minCommaOrCurly, false, true);
            charReader.updateCounter(minCommaOrCurly-1);
        }else if (JSON.indexOf(',', boolStart) != -1) {
            int comma = JSON.indexOf(',', boolStart);
            JSONObject.makeObject(boolStart, comma, false, true);
            charReader.updateCounter(comma-1);
        }else if (JSON.indexOf('}', boolStart) != -1) {
            int curlyClose = JSON.indexOf('}', boolStart);
            JSONObject.makeObject(boolStart, curlyClose, false, true);
            charReader.updateCounter(curlyClose-1);
        }else {
            charReader.throwException();
        }
    }

    private static void task () {
        switch (charReader.theCharacter()) {
            case "quotation":
                quote(leftHandSide);
                charReader.updateQuoteKeeper();
                break;
            case "curlyOpen":
                leftHandSide = true;
                int backUpCurlyStart = charReader.index();
                int leftSideListIndex = firstPartList.size()-1;
                if (charReader.index() != 0) {
                    curly();
                    addAtPositionAndClose(backUpCurlyStart, leftSideListIndex);
                }
                break;
            case "bracketOpen":
                leftHandSide = true;
                bracket();
                break;
            case "colon":
                leftHandSide = false;
                break;
            case "comma":
                leftHandSide = true;
                break;
            case "boolean":
                leftHandSide = true;
                booleanCase();
                JSONObject.makeSecondPart();
                break;
            case "number":
                leftHandSide = true;
                numberCase();
                JSONObject.makeSecondPart();
                break;
            default:
                break;
        }
    }

    static void deserializer () {
        while (charReader.nextExists()) {
            task();
        }
    }
}
